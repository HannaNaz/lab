#include "Person.h"
#include "conslib.h"
Person::Person(int i, int j): x(i), y(j) {};
void Person::set(int i, int j) { x = i; y = j; }
void Person::draw()
{
	printat(x, y, "_");
	printat(x+2, y, "_");
	printat(x + 1, y - 1, "|");
	printat(x + 1, y - 2, "|");
	printat(x , y - 2, "-");
	printat(x+2, y - 2, "-");
	printat(x + 1, y - 3, "0");
}
void Person::clear() 
{
	printat(x, y, " ");
	printat(x + 2, y, " ");
	printat(x + 1, y - 1, " ");
	printat(x + 1, y - 2, " ");
	printat(x, y - 2, " ");
	printat(x + 2, y - 2, " ");
	printat(x + 1, y - 3, " ");
};