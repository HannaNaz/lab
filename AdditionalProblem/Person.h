#pragma once
class Person
{
public:
	Person(int i, int j);
	void draw();
	void clear();
	void set(int i, int j);
private:
	int x; int y;
};