#include <iostream>
#include <list>
#include<vector>
#include <algorithm>
#include <iterator>

using namespace std;

class IntSequence
{
private:
	int value;
public:
	IntSequence(int intValue) :value(intValue) {};
	int operator()()
	{
		return value++;
	}
};
class Nth 
{
private:
	int nth;
	int count;
public:
	Nth(int n) :nth(n), count(0) {}
	bool operator()(int) 
	{
		return ++count == nth;
	}
};
int main() 
{
	list<int> coll = { 1,2,3,4,5,6,7,8,9,10 };
	for (auto elem : coll) cout << elem << " "; cout << endl;
	list <int>::iterator pos;
	pos = remove_if(coll.begin(), coll.end(), Nth(3));
	coll.erase(pos, coll.end());
	for (auto elem : coll) cout << elem<<" ";
/*
	list <int> coll;
	IntSequence seq(1);
	generate_n<back_insert_iterator<list<int>>, int, IntSequence>(back_inserter(coll), 4, seq);
	for (auto elem : coll) cout << elem; cout << endl;
	generate_n(back_inserter(coll),4, IntSequence(42));
	for (auto elem : coll) cout << elem << endl; cout << endl;
	generate_n(back_inserter(coll), 4, seq);
	for (auto elem : coll) cout << elem << " ";
	generate_n(back_inserter(coll), 4, seq);cout << endl;
	for (auto elem : coll) cout << elem << " ";
	vector<int> A = { 1,2,3,4 };*/
	//for_each(A.begin(), A.end(), IntSequence());
	//for_each(A.begin(), A.end(), [](int u) {cout << u;});
	system("pause");
	return 0;
}