#pragma once
#include <vector>
#include "points.h"
#include "lines.h"

using namespace std;


class Kirkpatrick 
{
public:
	Kirkpatrick(vector<Point> l);
	vector<Point> result(vector<vector<Point>> &st);
	vector<Point> turn(vector <Point> SomePoints, bool orientation);		//orientation - true if clockwise

protected:
	void BucketSort();
	vector<vector<Point>> states;
	vector <Point> points;
	vector <Point> convex;
	vector <Point> LeftPoints;
	vector <Point> RightPoints;
};