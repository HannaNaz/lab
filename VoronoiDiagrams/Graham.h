#pragma once
#include <vector>
#include "points.h"
#include "lines.h"

using namespace std;

class Graham
{
public:
	Graham(vector <Point>);
	vector<Point> sort();
	void findQ();
	vector <Point> result(vector<vector<Point>> &states);
	int ConvexNum;
	Point Q;  //the point with the lowest y-coordinate
private:
	vector <Point> points;
	Line L;
};