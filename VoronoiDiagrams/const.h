#pragma once
#include "points.h"
#include "lines.h"
#include "Graham.h"
#include "Jarvis.h"
#include "Kirkpatrick.h"
#include "recursive.h"
const int height = 500;
const int width = 500;
const int numPoints = 100;