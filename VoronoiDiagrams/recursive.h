#pragma once
#include "Jarvis.h"
#include "points.h"

class Recursive : public Jarvis
{
public:
	Recursive(vector <Point> v);
	vector <Point> result(vector <vector <Point>> &States);
	vector <Point> PointsAbove(vector <Point> vec, const Line &L, bool isAbove);
	vector <Point> recursion(vector <Point>, bool isAbove);
private:
	Point Left;
	Point Right;
	vector<Point> DownConvex;
	vector<Point> UpConvex;
	Point R1;
};