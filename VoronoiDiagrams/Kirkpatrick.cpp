#include "Kirkpatrick.h"
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

Kirkpatrick::Kirkpatrick(vector<Point> l) 
{
	points = l;
}

void Kirkpatrick::BucketSort() 
{
	int numBuckets = 500; int range = 500;
	vector<vector<Point>> sortedPoints;
	sortedPoints.resize(numBuckets);
	for (int i = 0; i < points.size(); ++i) 
	{
		int num = int(points[i].getY());
		sortedPoints[num].push_back(points[i]);
	}
	for (int i = 0; i < sortedPoints.size(); ++i)
	{
		sort(sortedPoints[i].begin(), sortedPoints[i].end());
	};
	for (int i = 0; i < sortedPoints.size(); ++i)
	{
		if (sortedPoints[i].size() >= 1)
		{
			LeftPoints.push_back(*(sortedPoints[i].begin()));
			RightPoints.push_back((sortedPoints[i])[sortedPoints[i].size() - 1]);
		};
	};
}
vector<Point> Kirkpatrick::result(vector<vector<Point>> &st)
{
	BucketSort();
	cout << "RightPoints turn\n";
	RightPoints = turn(RightPoints, false);
	convex = RightPoints;
	LeftPoints = turn(LeftPoints, true);	reverse(LeftPoints.begin(), LeftPoints.end());
	convex.insert(convex.end(), LeftPoints.begin(), LeftPoints.end());
	st = states;
	return convex;
}
vector<Point> Kirkpatrick::turn(vector <Point> SomePoints, bool orientation) 
{
	vector <Point> res; 
	Point PrevPoint = SomePoints[1];
	Line F(SomePoints[0], SomePoints[1]);
	double prev = F.angle(); double cur;
	res.push_back(SomePoints[0]); states.push_back(res); res.push_back(PrevPoint); 
	for (int i = 1; i < SomePoints.size(); ++i)
	{
		res.push_back(SomePoints[i]); states.push_back(res);
		Line L(PrevPoint, SomePoints[i]);
		cur = L.angle();
		while ((res.size() >= 2)&&((!orientation&&((cur - prev) <= 0)) || (orientation&&((cur - prev) >= 0))))
		{
			res.erase(res.end() - 2); vector <Point> oneState = points; points.insert(points.end(), res.begin(), res.end()); states.push_back(res);
				PrevPoint = (res[res.size() - 2]);
				Line Q(PrevPoint, *(--res.end()));
				L = Q;		
			cur = L.angle();
			if (res.size() <= 3) break;
			Line Temp(res[res.size() - 3], res[res.size() - 2]);
			prev = Temp.angle();
			
		}
		PrevPoint = SomePoints[i];
		prev = cur;
	}
	res.push_back(SomePoints[SomePoints.size() - 1]); states.push_back(res);
	return res;
}