#include "Graham.h"
#include <vector>
#include <map>
#include "points.h"
#include <iostream>   
#include "lines.h"

using namespace std;

Graham::Graham(vector <Point> p) :points(p) {};
vector<Point> Graham::sort()
{
	map <double, Point> PointsWithAngles;
	for (unsigned int i = 0; i < points.size(); ++i)
	{
		Line QA(points[i], Q);
		double angle = QA.angle();
		PointsWithAngles.insert((make_pair(angle, points[i])));
	}
	vector<Point> result;
	for (map<double, Point>::iterator it = PointsWithAngles.begin(); it != PointsWithAngles.end(); ++it) {
		result.push_back(it->second);
	}
	return result;
}
void Graham::findQ()
{
	Point Min = points[0];
	for (int i = 1; i < points.size(); ++i)
	{
		if (points[i].compareY(Min)<0) Min = points[i];
	}
	Q = Min;
}
vector<Point> Graham::result(vector<vector<Point>> &states)
{
	vector<Point> SortedPoints;

	findQ();
	cout << "\nQ = " << string(Q) << endl;
	vector<Point>::iterator FirstElem = points.begin();
	vector<Point>::iterator Temp;
	Temp = find(points.begin(), points.end(), Q);
	iter_swap(FirstElem, Temp);
	SortedPoints = sort();
	int N = SortedPoints.size();
	vector<Point> vec;
	vec.push_back(SortedPoints[N - 1]);
	int M = 1;
	vec.insert(vec.end(), SortedPoints.begin(), --SortedPoints.end());
	vector<Point> convex;
	convex.push_back(vec[0]); 
	convex.push_back(vec[1]);
	states.push_back(convex);
	for (int i = 2; i < vec.size(); ++i) 
	{
		while (convex[convex.size() - 2].ccw(convex[convex.size() - 1], vec[i]) < 0)
		{
			convex.pop_back();
			states.push_back(convex);
		}
		convex.push_back(vec[i]);		
		states.push_back(convex);
	}

	while (convex[convex.size() - 2].ccw(convex[convex.size() - 1], vec[0])<0)
		convex.pop_back(); states.push_back(convex);
	points = convex;
	return convex;
}

/*
cout << "Don`t worry" << endl;
for (int i = 0; i < SortedPoints.size(); ++i)
{
cout << string(SortedPoints[i]) << endl;
}*/









/*vector<Point> Graham::result()
{
	vector<Point> SortedPoints;

	findQ();
	cout << "\nQ = " << string(Q) << endl;
	vector<Point>::iterator FirstElem = points.begin();
	vector<Point>::iterator Temp;
	Temp = find(points.begin(), points.end(), Q);
	iter_swap(FirstElem, Temp);
	SortedPoints = sort();
	cout << "sort" << endl;
	for (int i = 0; i < SortedPoints.size(); ++i)
	{
		cout << "1: " << string(SortedPoints[i]) << endl;
	}
	int N = SortedPoints.size();
	vector<Point> vec;
	cout << "Help\n" << string(Q) << endl;
	vec.push_back(SortedPoints[N - 1]);
	int M = 1;
	vec.insert(vec.end(), SortedPoints.begin(), --SortedPoints.end());
	vector<Point> convex;
	cout << "Building convex\n";
	
	for (int i = 2; i < vec.size() - 1; ++i) {

		while ((vec[M - 1]).ccw(vec[M], vec[i]) <= 0)
		{
		//	cout << "Triple for M = "<<M<<":\n";
		//	cout << string(vec[M - 1]) << ", " << string(vec[M]) << ", " << string(vec[i]) << endl;
			if (M > 1)
			{
				M = M - 1;
			}
			else if (i == N) { break; }
			//else ++i;
		
		}
		M += 1;
		swap(points[M], points[i+1]);
		//vector<Point>::iterator TM = vec.begin() + M-1;
		//vector<Point>::iterator TI = vec.begin() + i-1;
		//iter_swap(TM, TI);
	};
	cout << "Result:" << endl;
	for (int i = 0; i <=M; ++i)
	{
		cout <<"2: "<< string(vec[i]) << endl;
	}
	ConvexNum = M;
	
return vec;
}*/