#pragma once
#include "Kirkpatrick.h"
#include "points.h"

using namespace std;
class Jarvis : public Kirkpatrick 
{
public:
	Jarvis(vector <Point> v);
	vector <Point> result(vector <vector <Point>> &States);
	vector<Point> turn(vector <Point> SomePoints, bool side);  //side - true if up, false if down
private:
	Point Left;
	Point Right;
	vector<Point> DownConvex;
	vector<Point> UpConvex;
};
