#include "Jarvis.h"
#include <algorithm>

using namespace std;
Jarvis::Jarvis(vector <Point> v) :Kirkpatrick( v){};

vector <Point> Jarvis::result(vector <vector <Point>> &St) 
{
	sort(points.begin(), points.end(), [](const Point &A, const Point &B) {return A>B;});
	Left = points[points.size() - 1];
	Right = points[0];
	Line L(Left, Right); Point Null(0, 0);
	for (int i = 0; i < points.size() ; ++i) 
	{
		if (L.oneSide(Null, points[i])) DownConvex.push_back(points[i]); else UpConvex.push_back(points[i]);
	}
	convex = turn(DownConvex, false); 	
	vector<Point> temp = turn(UpConvex, true); temp.push_back(Left);
	reverse(temp.begin(), temp.end());
    convex.insert(convex.end(), temp.begin(), temp.end() );
	St = states;
	return convex;
}
vector<Point> Jarvis::turn(vector <Point> SomePoints, bool side) 
{
	vector <Point> r; 
	r.push_back(Right);
	for (int i = 0; i < SomePoints.size(); ++i) 
	{
		states.push_back(r);
		bool flag = true;
		if (r[r.size() - 1] == SomePoints[i]) continue;
		Line Temp(r[r.size() - 1], SomePoints[i]); 
		for (int j = 0; (j <points.size())&&flag ; ++j)
		{
			if (Temp.otherSide(points[j], Left)&&(!(points[j]==SomePoints[i]))&&(!(points[j] == r[r.size() - 1]) ))
			{ 
				flag = false;
			};
			vector<Point> temp = r; 
			temp.push_back(SomePoints[i]); temp.push_back(points[j]); states.push_back(temp);
		}
		if (flag) 
		{
			r.push_back(SomePoints[i]); 
			vector<Point> temp = r; temp.push_back(SomePoints[i]);states.push_back(temp);states.push_back(temp);
		}
	}
	return r;
}