#include <GL/glut.h>
#include <iostream>
#include <vector>

#include <map>
#include "stdlib.h"
#include "const.h"
#include "time.h"
using namespace std;

vector<vector<Point>> states;
int currentState = 0;
vector<Point> points;
vector<Point> res;
Point Q;
void init2D(float r, float g, float b)
{
	glClearColor(r, g, b, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0.0, height, 0.0, width);
}
vector<Point> randomPoints()
{

	vector<Point> res;
		for (int i = 1; i < numPoints; ++i) 
		{
			Point T(rand() % height, rand() % width);
			res.push_back(T);
		}
		return res;
}

void onIdle()
{
	if (currentState < states.size() - 1)
	{
		currentState++;
		Sleep(500);
	}
		glutPostRedisplay();
	//};
}

void displayPoints(void)
{
	
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0, 0.0, 0.0);
	glPointSize(5);
	glEnable(GL_POINT_SMOOTH);
	glBegin(GL_POINTS);
	for (int i = 0; i < points.size(); i++)
	{
		glVertex2i(points[i].getX(), points[i].getY());
	}
	glEnd();
	glColor3f(0.0, 0.0, 0.5);
	glLineWidth(1.1);
		glLineStipple(2, 0xAAAA);  
		glEnable(GL_LINE_STIPPLE); 
		glBegin(GL_LINES);
		for (int i = 0; i < res.size() - 1; ++i)
		{
			glVertex2i(res[i ].getX(), res[i].getY());
			glVertex2i(res[i+1].getX(), res[i+1].getY());
		}
		glEnd();
		Point A(0, 0);
		if (!(Q == A)) {
			glBegin(GL_LINES);
			for (int i = 0; i < points.size(); i++)
			{
				glVertex2i(Q.getX(), Q.getY());
				glVertex2i(points[i].getX(), points[i].getY());
			}
			glEnd();
		};
	glLineWidth(1);
	if (currentState >= 0) {
		auto r = states[currentState];
		glColor3f(0.0, 0.0, 0.0);
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		glLineWidth(1);
		glDisable(GL_LINE_STIPPLE);
		glBegin(GL_LINES);
		for (int i = 0; i < r.size() - 1; i++)
		{
			//cout << string(res[i]) << ": " << string(res[i + 1]) << endl;
			if (r[i] == r[i + 1]) continue;
			glVertex2i(r[i].getX(), r[i].getY());
			glVertex2i(r[i + 1].getX(), r[i + 1].getY());
		}
		glVertex2i(r[r.size() - 1].getX(), r[r.size() - 1].getY());
		glVertex2i(r[0].getX(), r[0].getY());
		glEnd();
	}
		glFlush();
	
}

int main(int argc, char *argv[])
{
	
	srand(time(0));
	points = randomPoints();
	Recursive recursive(points);
	 res = recursive.result(states);
	//Jarvis jarvis(points);
	// res = jarvis.result(states);
	currentState = 0;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(height, width);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("VoronoiDiagram");
	init2D(1.0, 1.0, 1.0);
	glutDisplayFunc(displayPoints);
	glutIdleFunc(onIdle);

	glutMainLoop();/*
	Point H1(1, 30);
	Point H2(10, 20);
	Point H3(10, 30), H4(10, 40), H5(20, 10), H6(20, 50), H7(30, 10), H8(30, 60), H9(40, 10), H10(40, 60), H11(50, 10), H12(50, 60), H13(60, 20), H14(60, 30), H15(60, 40), H16(70, 30);
	vector<Point> v;
	points = { H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11, H12,H13,H14,H15,H16 };

	Recursive Test(points);
	res = Test.result(states);

	
	currentState = 0;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(height, width);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("VoronoiDiagram");
	init2D(1.0, 1.0, 1.0);
	glutDisplayFunc(displayPoints);
	glutIdleFunc(onIdle);
	glutMainLoop();*/


	system("pause");
	return 0;
}


/*	Point A(4, 1);
Point B(4, 3);
Point C(4, 4);
Point D(6, 2);
Point N(7, 4);
Point K(8, 6);
Point K1(2, 10);
//Point K1(7,20);
Point E(4, 5);
Point M(1, 5);*/