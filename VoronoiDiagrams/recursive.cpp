#include "recursive.h"
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

Recursive::Recursive(vector <Point> v):Jarvis(v){};
vector <Point> Recursive::recursion(vector <Point> SomePoints,  bool above)
{
	vector <Point> result;
	if (SomePoints.size() ==0) return SomePoints;
	sort(SomePoints.begin(), --SomePoints.end(), [](const Point &A, const Point &B) {return A <= B;});
	Point LN = SomePoints[0];
	Point RN = (SomePoints[SomePoints.size() - 1]);
	Line LR(LN, RN);
	Point H1;
	H1 = *(max_element(SomePoints.begin(), SomePoints.end(), [=](const Point &A, const Point &B)
	{if (above&&LR.under(A) || above&&LR.under(B) || !above && !LR.under(B)|| !above&&!LR.under(A)) return false;
	return (above && (LR.distance(A) > LR.distance(B)) || !above && (LR.distance(A) < LR.distance(B)));}));
	Line LH1(LN, H1); Line RH1(RN, H1); 
	vector <Point> S11 = PointsAbove(SomePoints, LH1, above); vector <Point> S12 = PointsAbove(SomePoints, RH1, above);
	vector <Point> state;
	result = recursion(S12, above);
	vector<Point> temp = recursion(S11, above);
	result.push_back(H1);
	result.insert(result.end(), temp.begin(), temp.end());
	states.insert(states.end(), result);
	return result;
};
vector <Point> Recursive::result(vector <vector <Point>> &States)
{
	vector <Point> result;
	sort(points.begin(), points.end(), [](const Point &A, const Point &B) {return A <= B;});
	Left = points[0]; Right = points[points.size() - 1]; R1 = Right;
	cout << "Left " << string(Left) << "; Right " << string(Right) << endl;
	Line LR(Left, Right);
	vector <Point> AboveLR = PointsAbove(points, LR, true);
	AboveLR.push_back(Left); AboveLR.push_back(Right);
	vector <Point> UnderLR = PointsAbove(points, LR, false);
	UnderLR.push_back(Left); UnderLR.push_back(Right);
	result.push_back(Left); 
	vector<Point> temp = recursion(UnderLR, false);
	result.insert(result.end(), temp.begin(), temp.end()); result.push_back(Right);
	if(result.size()!=0)sort(result.begin(), result.end(), [](const Point &A, const Point &B) {return A > B;});
	temp = recursion(AboveLR, true); reverse(temp.begin(), temp.end());
	result.insert(result.end(), temp.begin(), temp.end()); 
	result.erase(unique(result.begin(), result.end()), result.end()); 	result.push_back(R1);
	States = states;
	return result;
}
vector <Point> Recursive::PointsAbove(vector <Point> vec, const Line &L, bool isAbove)
{
	vector <Point> Res;
	for (int i = 1; i <vec.size(); ++i)
	{
		if (!L.under(vec[i])&&isAbove) Res.push_back(vec[i]); else
			if (L.under(vec[i])&&!isAbove) Res.push_back(vec[i]);
	}
	return Res;
}
/*	vector <Point> result;
	if (points.size() <= 2) return points;
	sort(points.begin(), points.end(), [](const Point &A, const Point &B) {return A <= B;});
	Left = points[0]; Right = points[points.size() - 1]; Line LR(Left, Right);
	cout << "Left = " << string(Left) <<" and Right = "<<string(Right)<< endl;
	vector <Point> AboveLR = PointsAbove(points, LR, true);
	vector <Point> UnderLR = PointsAbove(points, LR, false);
	Point H1 = *(max_element(AboveLR.begin(), AboveLR.end(), [=](const Point &A, const Point &B)
	{return LR.distance(A) < LR.distance(B);}));
	Point H2 = *(max_element(UnderLR.begin(), UnderLR.end(), [=](const Point &A, const Point &B)
	{return LR.distance(A) < LR.distance(B);}));
	Line LH1(Left, H1);
	Line RH2(Right, H2); vector <Point> Temp;
	Temp = PointsAbove(AboveLR, LH1, true);
	vector <Point> state; state.push_back(Left); state.push_back(Right);
//	if (points.size() == 2) return state;
	States.push_back(state);
	vector <Point> S11; S11.push_back(Left);  S11.insert(S11.end(), Temp.begin(), Temp.end()); S11.push_back(H1);
	vector <Point> S12; S12.push_back(Left);  S12 = PointsAbove(AboveLR, LH1, false); S12.push_back(H1);
	vector <Point> S22 = PointsAbove(UnderLR, RH2, true); S22.push_back(Right);  S22.push_back(H2);
	vector <Point> S21 = PointsAbove(UnderLR, RH2, false); S21.push_back(Right);  S21.push_back(H2);
	result.push_back(Left);
	Recursive R11(S11);
	Temp = R11.result(States);
	result.insert(result.end(), Temp.begin(), Temp.end());
	result.push_back(H1);
	Recursive R12(S12); Temp = R12.result(States); 
	result.insert(result.end(), Temp.begin(), Temp.end());
	Recursive R21(S21);
	Temp = R21.result(States);
	result.insert(result.end(), Temp.begin(), Temp.end());
	result.push_back(H2);
	Recursive R22(S22); Temp = R22.result(States); result.insert(result.end(), Temp.begin(), Temp.end());
	cout << endl;
	for (int i = 0; i < Temp.size(); ++i)
	{
		cout << string(Temp[i]) << endl;
	}
	cout << "\n Result:\n";
	for (int i = 0; i < result.size(); ++i) 
	{
		cout << string(result[i]) << endl;
	}
	cout <<"H1 = "<< string(H1) <<" H2 = "<<string(H2)<< endl;
	return result;*/